from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Resource, Api, fields
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://mymobiconf:mymobiconf@feedback-mysql/mymobiconf'


api = Api(app)

db = SQLAlchemy(app)


def register_namespaces():
    from resources import questionario_ns, opiniao_ns, nota_ns

    api.add_namespace(questionario_ns)
    api.add_namespace(opiniao_ns)
    api.add_namespace(nota_ns)


register_namespaces()
