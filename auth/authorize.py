import sys
from http import HTTPStatus

import requests

from auth.AuthLevel import AuthLevel


def check_authorization(event, auth_level=AuthLevel.PARTICIPANTE, base_url="poc.primary157.com.br:84", **kwargs):
    assert kwargs["access_token"] is not None
    assert kwargs["uid"] is not None
    token = kwargs["access_token"]
    uid = kwargs["uid"]
    print(f"requested http://{base_url}/evento/{event}/{auth_level.value}/{uid}?access_token={token}&uid={uid}",
          file=sys.stderr)
    response = requests.get(f"http://{base_url}/evento/{event}/{auth_level.value}/{uid}?access_token={token}&uid={uid}")
    print(f"response.status_code = {response.status_code}", file=sys.stderr)
    if response.ok or response.status_code == HTTPStatus.OK:
        return True
    elif response.status_code == HTTPStatus.UNAUTHORIZED:
        raise ValueError(
            f"Authentication Failed: wrong access_token ({kwargs['access_token']}) and uid({kwargs['uid']}) pair!")
    else:
        raise RuntimeError("Something went wrong during authorization check!")
