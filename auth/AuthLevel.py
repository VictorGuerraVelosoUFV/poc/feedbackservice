from enum import Enum


class AuthLevel(Enum):
    PARTICIPANTE = "participante"
    ADMIN = "admin"
