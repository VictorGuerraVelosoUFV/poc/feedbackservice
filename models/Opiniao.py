import datetime

from app import db
from models.utils.decorators import add_schema, db_to_api


@add_schema
@db_to_api(blacklist=["id"])
class Opiniao(db.Model):
    # __tablename__ = 'opiniao'

    id = db.Column(db.Integer, name='ID', primary_key=True)  # Field name made lowercase.

    id_evento = db.Column(db.Integer, name="id_evento", nullable=False)

    auth_user_ID = db.Column(db.String(36), name='auth_user_ID', nullable=True)

    opiniao = db.Column(db.String(500), name="opiniao", nullable=False)
    anonimo = db.Column(db.Boolean, name="anonimo", nullable=False)
