import datetime

from app import db
from models.utils.decorators import add_schema, db_to_api


@add_schema
@db_to_api(blacklist=["id"])
class Questionario(db.Model):
    # __tablename__ = 'questionario'
    id = db.Column(db.Integer, name='ID', primary_key=True)  # Field name made lowercase.

    id_evento = db.Column(db.Integer, name="id_evento", nullable=False)

    id_atividade = db.Column(db.Integer, name='id_atividade', nullable=True)

    nome = db.Column(db.String(300), name="nome", nullable=False)
    disponivel = db.Column(db.Boolean, name="disponivel", nullable=False)

    pergunta = db.relationship('Pergunta', cascade="all", backref=db.backref('questionario', lazy=True))
