import datetime

from app import db
from models.utils.decorators import add_schema, db_to_api


@add_schema
@db_to_api(blacklist=[])
class NotaAtividade(db.Model):
    # __tablename__ = 'nota_atividade'
    id_atividade = db.Column(db.Integer, name='id_atividade', primary_key=True, autoincrement=False)

    auth_user_ID = db.Column(db.String(36), name='auth_user_ID', primary_key=True, autoincrement=False)

    data = db.Column(db.DateTime, name='data')  # Field name made lowercase.
    nota_conforto_termico = db.Column(db.Integer, name='nota_conforto_termico', nullable=True)
    nota_conforto_sonoro = db.Column(db.Integer, name='nota_conforto_sonoro', nullable=True)
    nota_geral = db.Column(db.Integer, name='nota_geral', nullable=True)
