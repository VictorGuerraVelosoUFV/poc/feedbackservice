import datetime

from app import db
from models.utils.decorators import add_schema, db_to_api


@add_schema
@db_to_api(blacklist=["id"])
class Pergunta(db.Model):
    # __tablename__ = 'questionario'
    id = db.Column(db.Integer, name='ID', primary_key=True)  # Field name made lowercase.

    pergunta = db.Column(db.String(350), name="pergunta", nullable=False)

    id_questionario = db.Column(db.Integer, db.ForeignKey("questionario.ID"), name="ID_QUESTIONARIO", nullable=False)

    resposta = db.relationship('Resposta', cascade="all", backref=db.backref('pergunta', lazy=True))
