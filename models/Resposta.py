import datetime

from app import db
from models.utils.decorators import add_schema, db_to_api


@add_schema
@db_to_api(blacklist=[])
class Resposta(db.Model):
    # __tablename__ = 'resposta'
    id_pergunta = db.Column(db.Integer, db.ForeignKey("pergunta.ID"), name='ID_PERGUNTA', primary_key=True,
                            autoincrement=False)

    auth_user_ID = db.Column(db.String(36), name='auth_user_ID', primary_key=True, autoincrement=False)

    hora = db.Column(db.Time, name='hora', nullable=False)  # Field name made lowercase.
    resposta = db.Column(db.String(150), name='resposta', nullable=False)
