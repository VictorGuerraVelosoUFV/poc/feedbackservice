import sys
from datetime import datetime, date, time

import flask
from flask_restplus import abort, Namespace
from sqlalchemy.exc import DatabaseError

from app import db, Resource, request
from http import HTTPStatus

from auth.AuthLevel import AuthLevel
from auth.authorize import check_authorization
from models import Pergunta, Questionario, Resposta
from auth.authenticate import authenticate_or_abort
from resources.utils.EventService import get_atividade, get_evento

questionario_ns = Namespace(name="questionario", description="Rotas relacionadas a questionarios", path="/questionario")


@questionario_ns.route("/")
class QuestionariosResource(Resource):
    @questionario_ns.expect(Questionario.RequestModel)
    @questionario_ns.marshal_with(Questionario.ResponseModel)
    @questionario_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @questionario_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self):
        questionario_json = request.get_json()
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(questionario_json["id_evento"], AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        questionario, err = Questionario.Schema().load(questionario_json, session=db.session)
        if err:
            abort(HTTPStatus.BAD_REQUEST, err)
        db.session.add(questionario)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return Questionario.query.order_by(Questionario.id.desc()).first()


@questionario_ns.route('/<id>')
class QuestionarioResource(Resource):
    @questionario_ns.marshal_with(Questionario.ResponseModel)
    @questionario_ns.response(code=HTTPStatus.NOT_FOUND, description="Não foi possível encontrar o questionário")
    def get(self, id):
        questionario = Questionario.query.get(id)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(questionario.id_evento, AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError as e:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR, e)
        except AttributeError:
            abort(HTTPStatus.NOT_FOUND)
        if questionario is None:
            return abort(HTTPStatus.NOT_FOUND)
        return questionario

    def delete(self, id):
        questionario = Questionario.query.get(id)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(id, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        if questionario is None:
            print("Questionario não encontrado")
            abort(HTTPStatus.NOT_FOUND)
        db.session.delete(questionario)
        db.session.commit()


@questionario_ns.route('/evento/<id_evento>')
class QuestionariosEventoResource(Resource):
    @questionario_ns.marshal_with(Questionario.ResponseModel)
    @questionario_ns.response(code=HTTPStatus.NOT_FOUND, description="Não foi possível encontrar o evento")
    def get(self, id_evento):
        questionarios = Questionario.query.filter_by(id_evento=id_evento).all()
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(id_evento, AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        return questionarios


@questionario_ns.route("/<id>/atividade/<id_atividade>")
class AssociacaoQuestionarioAtividadeResource(Resource):
    @questionario_ns.marshal_with(Questionario.ResponseModel)
    @questionario_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @questionario_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self, id, id_atividade):
        questionario = Questionario.query.get(id)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(questionario.id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        try:
            _ = get_atividade(id_atividade, token, uid)
        except (ValueError, RuntimeError):
            abort(HTTPStatus.NOT_FOUND, f"Not found atividade with id({id_atividade})")
        questionario.id_atividade = id_atividade
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return questionario

#######################################################################################################################
# Endpoints Pergunta
#######################################################################################################################


@questionario_ns.route("/<id_questionario>/pergunta")
class PerguntasQuestionarioResource(Resource):
    @questionario_ns.marshal_with(Pergunta.ResponseModel)
    def get(self, id_questionario):
        questionario = Questionario.query.get(id_questionario)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(questionario.id_evento, AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        perguntas = Pergunta.query.filter_by(id_questionario=id_questionario).all()
        authenticate_or_abort(flask.request.args.get, abort)
        return perguntas

    @questionario_ns.expect(Pergunta.RequestModel)
    @questionario_ns.marshal_with(Pergunta.ResponseModel)
    @questionario_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @questionario_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self, id_questionario):
        pergunta_json = request.get_json()

        questionario = Questionario.query.get(id_questionario)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(questionario.id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)

        pergunta_json["id_questionario"] = id_questionario
        pergunta, err = Pergunta.Schema().load(pergunta_json)
        if err:
            abort(HTTPStatus.BAD_REQUEST, err)

        pergunta.questionario = Questionario.query.get(id_questionario)
        db.session.add(pergunta)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return Pergunta.query.order_by(Pergunta.id.desc()).first()


@questionario_ns.route("/pergunta/<id>")
class PerguntaResource(Resource):
    @questionario_ns.marshal_with(Pergunta.ResponseModel)
    def get(self, id):
        pergunta = Pergunta.query.get(id)

        if pergunta is None:
            return abort(HTTPStatus.NOT_FOUND)

        questionario = Questionario.query.get(pergunta.id_questionario)

        if questionario is None:
            abort(HTTPStatus.NOT_FOUND)

        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(questionario.id_evento, AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)

        return pergunta

    def delete(self, id):
        pergunta = Pergunta.query.get(id)

        if pergunta is None:
            return abort(HTTPStatus.NOT_FOUND)

        questionario = Questionario.query.get(pergunta.id_questionario)
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(questionario.id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)

        db.session.delete(pergunta)
        db.session.commit()


#######################################################################################################################
# Endpoints Resposta
#######################################################################################################################

@questionario_ns.route("/pergunta/<id_pergunta>/respostas")
class RespostasResource(Resource):
    @questionario_ns.marshal_with(Resposta.ResponseModel)
    def get(self, id_pergunta):
        pergunta = Pergunta.query.get(id_pergunta)

        if pergunta is None:
            return abort(HTTPStatus.NOT_FOUND)

        questionario = Questionario.query.get(pergunta.id_questionario)

        if questionario is None:
            abort(HTTPStatus.NOT_FOUND)

        resposta = Resposta.query.filter_by(id_pergunta=id_pergunta).all()
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(questionario.id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        return resposta

    @questionario_ns.expect(Resposta.RequestModel)
    @questionario_ns.marshal_with(Resposta.ResponseModel)
    @questionario_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @questionario_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self, id_pergunta):
        pergunta = Pergunta.query.get(id_pergunta)

        if pergunta is None:
            return abort(HTTPStatus.NOT_FOUND)

        questionario = Questionario.query.get(pergunta.id_questionario)

        if questionario is None:
            abort(HTTPStatus.NOT_FOUND)

        resposta_json = request.get_json()
        resposta_json["id_pergunta"] = id_pergunta
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(questionario.id_evento, AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)

        # Not allowed to create Resposta before activity starts and after event finishes

        if questionario.id_atividade is not None:
            atv = get_atividade(questionario.id_atividade, token, uid)
            dt = datetime.combine(date.fromisoformat(atv["data"]), time.fromisoformat(atv["hora_inicio"]))
            if dt < datetime.now():
                abort(HTTPStatus.METHOD_NOT_ALLOWED,
                      "You shouldn't create an evaluation grade neither before the activity occurrence nor after the end of the event")
        ev = get_evento(questionario.id_evento, token, uid)
        if datetime.now().date() > date.fromisoformat(ev["data_fim"]):
            abort(HTTPStatus.METHOD_NOT_ALLOWED,
                  "You shouldn't create an evaluation grade neither before the activity occurrence nor after the end of the event")

        resposta, err = Resposta.Schema().load(resposta_json)
        resposta.pergunta = pergunta
        if err:
            abort(HTTPStatus.BAD_REQUEST, err)
        resposta.evento = Questionario.query.get(id_pergunta)
        db.session.add(resposta)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)

        return Resposta.query.filter_by(auth_user_ID=resposta_json["auth_user_ID"], pergunta=pergunta).first()


@questionario_ns.route("/pergunta/<id_pergunta>/resposta")
class RespostaResource(Resource):
    @questionario_ns.marshal_with(Resposta.ResponseModel)
    def get(self, id_pergunta):
        pergunta = Pergunta.query.get(id_pergunta)

        if pergunta is None:
            return abort(HTTPStatus.NOT_FOUND)

        questionario = Questionario.query.get(pergunta.id_questionario)

        if questionario is None:
            abort(HTTPStatus.NOT_FOUND)

        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        resposta = Resposta.query.filter_by(id_pergunta=id_pergunta, auth_user_ID=uid).first()
        try:
            check_authorization(questionario.id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        if resposta is None:
            abort(HTTPStatus.NOT_FOUND)
        return resposta

    def delete(self, id_pergunta):
        pergunta = Pergunta.query.get(id_pergunta)

        if pergunta is None:
            return abort(HTTPStatus.NOT_FOUND)

        questionario = Questionario.query.get(pergunta.id_questionario)

        if questionario is None:
            abort(HTTPStatus.NOT_FOUND)

        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        resposta = Resposta.query.filter_by(id_pergunta=id_pergunta, auth_user_ID=uid).first()
        try:
            check_authorization(questionario.id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, AssertionError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        except RuntimeError:
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)
        if resposta is None:
            print("Resposta não encontrada")
            abort(HTTPStatus.NOT_FOUND)
        db.session.delete(resposta)
        db.session.commit()
