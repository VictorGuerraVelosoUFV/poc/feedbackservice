import sys
from datetime import datetime, date, time

import flask
from flask_restplus import abort, Namespace
from sqlalchemy.exc import DatabaseError

from app import db, Resource, request
from http import HTTPStatus

from auth.AuthLevel import AuthLevel
from auth.authorize import check_authorization
from models import NotaAtividade
from auth.authenticate import authenticate_or_abort
from resources.utils.EventService import get_evento, get_atividade

nota_ns = Namespace(name="nota", description="Rotas relacionadas a Nota de atividades", path="/atividade/nota")


@nota_ns.route("/")
class NotasResource(Resource):
    @nota_ns.expect(NotaAtividade.RequestModel)
    @nota_ns.marshal_with(NotaAtividade.ResponseModel)
    @nota_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @nota_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self):  # Upsert
        nota_json = request.get_json()
        atv = ev = {}
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            atv = get_atividade(nota_json["id_atividade"], token, uid)
            ev = get_evento(atv["id_evento"], token, uid)
            check_authorization(atv["id_evento"], AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (RuntimeError, ValueError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        nota = NotaAtividade.query.filter_by(id_atividade=nota_json["id_atividade"],
                                             auth_user_ID=nota_json["auth_user_ID"]).first()
        print(f"Passou aqui 1: {nota}", file=sys.stderr)
        dt = datetime.combine(date.fromisoformat(atv["data"]), time.fromisoformat(atv["hora_inicio"]))
        if dt < datetime.now() or datetime.now().date() > date.fromisoformat(ev["data_fim"]):
            abort(HTTPStatus.METHOD_NOT_ALLOWED,
                  "You shouldn't create an evaluation grade neither before the activity occurrence nor after the end of the event")
        if nota is None:  # insert
            nova_nota, err = NotaAtividade.Schema().load(nota_json, session=db.session)
            if err:
                print(err, file=sys.stderr)
                abort(HTTPStatus.BAD_REQUEST, err)
            db.session.add(nova_nota)
        else:  # update
            nota.data = nota_json["data"]
            NotaAtividade.nota_conforto_termico = nota_json["nota_conforto_termico"]
            NotaAtividade.nota_conforto_sonoro = nota_json["nota_conforto_sonoro"]
            NotaAtividade.nota_geral = nota_json["nota_geral"]
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return NotaAtividade.query.filter_by(id_atividade=nota_json["id_atividade"],
                                             auth_user_ID=nota_json["auth_user_ID"]).first()


@nota_ns.route("/<id_atividade>")
class NotaResource(Resource):
    @nota_ns.marshal_with(NotaAtividade.ResponseModel)
    def get(self, id_atividade):
        nota = NotaAtividade.query.filter_by(id_atividade=id_atividade).all()
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        atv = get_atividade(nota[0].id_atividade, token, uid)
        check_authorization(atv["id_evento"], AuthLevel.ADMIN, access_token=token, uid=uid)
        if nota is None:
            abort(HTTPStatus.NOT_FOUND)
        return nota
