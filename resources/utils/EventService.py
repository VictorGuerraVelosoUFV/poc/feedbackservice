import sys
from http import HTTPStatus

import requests


def get_evento(id_evento: int, token: str, uid: str) -> dict:
    print(f"http://poc.primary157.com.br:83/evento/{id_evento}?access_token={token}&uid={uid}", file=sys.stderr)
    response = requests.get(f"http://poc.primary157.com.br:83/evento/{id_evento}?access_token={token}&uid={uid}")
    if response.ok:
        return response.json()
    print(f"response.status_code = {response.status_code}", file=sys.stderr)
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        raise ValueError(f"Authentication Failed: wrong access_token ({token}) and uid({uid}) pair!")
    else:
        raise RuntimeError("Something went wrong during authorization check!")


def get_atividade(id_atividade: int, token: str, uid: str) -> dict:
    print(f"http://poc.primary157.com.br:83/atividade/{id_atividade}?access_token={token}&uid={uid}", file=sys.stderr)
    response = requests.get(f"http://poc.primary157.com.br:83/atividade/{id_atividade}?access_token={token}&uid={uid}")
    if response.ok:
        return response.json()
    print(f"response.status_code = {response.status_code}", file=sys.stderr)
    if response.status_code == HTTPStatus.UNAUTHORIZED:
        raise ValueError(f"Authentication Failed: wrong access_token ({token}) and uid({uid}) pair!")
    else:
        raise RuntimeError("Something went wrong during authorization check!")