import sys

import flask
from flask_restplus import abort, Namespace
from sqlalchemy.exc import DatabaseError

from app import db, Resource, request
from http import HTTPStatus

from auth.AuthLevel import AuthLevel
from auth.authorize import check_authorization
from models import Opiniao
from auth.authenticate import authenticate_or_abort

opiniao_ns = Namespace(name="opiniao", description="Rotas relacionadas a opiniões", path="/opiniao")


@opiniao_ns.route("/")
class OpinioesResource(Resource):

    @opiniao_ns.expect(Opiniao.RequestModel)
    @opiniao_ns.marshal_with(Opiniao.ResponseModel)
    @opiniao_ns.response(code=HTTPStatus.UNAUTHORIZED, description="")
    @opiniao_ns.response(code=HTTPStatus.BAD_REQUEST, description="")
    def post(self):
        opiniao_json = request.get_json()
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(opiniao_json["id_evento"], AuthLevel.PARTICIPANTE, access_token=token, uid=uid)
        except (ValueError, RuntimeError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        opiniao, err = Opiniao.Schema().load(opiniao_json, session=db.session)
        if err:
            print(err, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST, err)
        db.session.add(opiniao)
        try:
            db.session.commit()
        except DatabaseError as e:
            print(e, file=sys.stderr)
            abort(HTTPStatus.BAD_REQUEST)
        return Opiniao.query.order_by(Opiniao.id.desc()).first()


@opiniao_ns.route("/<id_evento>")
class OpiniaoResource(Resource):
    @opiniao_ns.marshal_with(Opiniao.ResponseModel)
    def get(self, id_evento):
        opinioes = Opiniao.query.filter_by(id_evento=id_evento).all()
        token, uid = authenticate_or_abort(flask.request.args.get, abort)
        try:
            check_authorization(id_evento, AuthLevel.ADMIN, access_token=token, uid=uid)
        except (ValueError, RuntimeError) as e:
            abort(HTTPStatus.UNAUTHORIZED, e)
        return opinioes
